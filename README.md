# Custom build image for Jenkins

This is an extension of the default build image provided by CERN for Jenkins, installing additional packages.

Check the `Dockerfile` and `.gitlab-ci.yml` files of this repository for more information.

## How to use

* Clone this repository
* Edit the Dockerfile
* Commit and push.
* Wait a bit, Docker images are built by the gitlab CI instance.
* Once done, go to the `Registry` tabs to see the list of images available.

By default, our Jenkins instance use the `slc6` image. The new image will be used
automatically for the next built.
